/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.functions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import me.ryvix.claimcommands.data.Flags;
import me.ryvix.claimcommands.ClaimCommands;

public class SQLFunctions {

	private static ClaimCommands plugin;

	public SQLFunctions(ClaimCommands plugin) {
		SQLFunctions.plugin = plugin;
	}

	/**
	 * Insert flag into database
	 *
	 * @param claimid
	 * @param flag
	 * @param value
	 * @throws SQLException
	 */
	public void insert(Long claimid, String flag, String value) throws SQLException {
		Flags f = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).ieq("flag", flag).findUnique();

		if (f == null) {
			f = new Flags();
			f.setClaimid(claimid.intValue());
			f.setFlag(flag);
			f.setValue(value);
			plugin.getDatabase().save(f);
		}
	}

	/**
	 * Delete flag from database
	 *
	 * @param claimid
	 * @param flag
	 * @throws SQLException
	 */
	public void delete(Long claimid, String flag) throws SQLException {
		Flags f = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).ieq("flag", flag).findUnique();

		if (f == null) {
			return;
		}

		plugin.getDatabase().delete(f);

		plugin.getDatabase().save(f);
	}

	/**
	 * Delete flag from database by player
	 *
	 * @param claimid
	 * @param flag
	 * @param value
	 * @throws SQLException
	 */
	public void delete(Long claimid, String flag, String value) throws SQLException {
		Flags f = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).ieq("flag", flag).ieq("value", value).findUnique();

		if (f == null) {
			return;
		}
	
		plugin.getDatabase().delete(f);

		plugin.getDatabase().save(f);
	}

	/**
	 * Select all flags a claim has
	 *
	 * @param claimid
	 * @return List<String>
	 * @throws SQLException
	 */
	public List<String> select(Long claimid) throws SQLException {
		List<Flags> flags = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).findList();
		List<String> result = new ArrayList<>();
		if (!flags.isEmpty()) {
			for (Flags f : flags) {
				result.add(f.getFlag());
			}
		}

		return result;
	}

	/**
	 * Select all values from a flag
	 *
	 * @param claimid
	 * @param flag
	 * @return List<String>
	 * @throws SQLException
	 */
	public List<String> select(Long claimid, String flag) throws SQLException {
		List<Flags> flags = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).ieq("flag", flag).findList();
		List<String> result = new ArrayList<>();
		if (!flags.isEmpty()) {
			for (Flags f : flags) {
				result.add(f.getValue());
			}
		}

		return result;
	}

	/**
	 * Select one flag a claim has - with player
	 *
	 * @param claimid
	 * @param flag
	 * @param player
	 * @return List<String>
	 * @throws SQLException
	 */
	public List<String> select(Long claimid, String flag, String player) throws SQLException {
		List<Flags> flags = plugin.getDatabase().find(Flags.class).where().eq("claimid", claimid.intValue()).ieq("flag", flag).ieq("value", player).findList();
		List<String> result = new ArrayList<>();
		if (!flags.isEmpty()) {
			for (Flags f : flags) {
				result.add(f.getFlag());
			}
		}

		return result;
	}
}
