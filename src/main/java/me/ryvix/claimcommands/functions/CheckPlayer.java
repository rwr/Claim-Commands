/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.functions;

import java.util.UUID;
import org.bukkit.Location;

/**
 * CheckPlayer object This will store player UUIDs and their last locations.
 * Used for saving their locations for each check to see where they were.
 */
public class CheckPlayer {

	private UUID uuid;
	private Location location;

	// constructor
	public CheckPlayer(UUID u, Location l) {
		this.uuid = u;
		this.location = l;
	}

	// get player UUID
	public UUID getUuid() {
		return uuid;
	}

	// set player UUID
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	// get player's location
	public Location getLocation() {
		return location;
	}

	// set player's location
	synchronized void setLocation(Location location) {
		this.location = location;
	}
}
