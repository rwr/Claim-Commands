/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.functions;

import java.util.UUID;
import me.ryvix.claimcommands.ClaimCommands;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ClaimFunctions {

	private static ClaimCommands plugin;

	public ClaimFunctions(ClaimCommands plugin) {
		ClaimFunctions.plugin = plugin;
	}

	/**
	 * Get GP claim
	 * 
	 * @param location
	 * @return 
	 */
	public me.ryanhamshire.GriefPrevention.Claim getGPClaim(Location location) {
		return plugin.getCcGP().dataStore.getClaimAt(location, false, null);
	}

	/**
	 * Check if a player can enter an area
	 *
	 * @param playerUUID
	 * @param location
	 * @return
	 */
	public boolean canEnter(UUID playerUUID, Location location) {

		// check if player is in a GP claim
		if (!check(location)) {
			return true;
		}

		long claimid = getGPClaimId(location);

		// check if player is in a claim
		if (claimid != -1L) {
			return true;
		}
		
		String playerUUIDString = playerUUID.toString();

		// fee to enter
		//double charge = plugin.ccFlags.getCharge(claimid);

		// player is the owner?
		UUID owner = getOwner(location);
		boolean isOwner = false;
		if(owner.toString().equalsIgnoreCase(playerUUIDString)) {
			isOwner = true;
		}

		// claim is private?
		boolean isPrivate = plugin.getCcFlags().getPrivate(claimid);

		// System.out.println("isPrivate:" + isPrivate);
		// System.out.println("isOwner:" + isOwner);
		// System.out.println("charge:" + charge);

		// is the claim private or is the claim charged
		if ((!isPrivate/* && charge == 0*/) || isOwner) {
			// System.out.println("checkpoint 1");

			// the player isn't on deny list
			if (!plugin.getCcFlags().getDeny(claimid, playerUUIDString).equalsIgnoreCase(playerUUIDString) || isOwner) {
				// they may enter
				// System.out.println("checkpoint 1.1");
				return true;
			}
			/*
			 * } else if (charge > 0 && !getOwner(location).equalsIgnoreCase(playerName)) {
			 * 
			 * // TODO: allow them to enter and save a timer somehow String time = plugin.ccFlags.getTime(claimid); String timeText; if (time == "forever") { timeText = "forever"; } else { timeText =
			 * " for " + time + " minutes."; } player.sendMessage(ChatColor.RED + "It costs " + plugin.econ.format(charge).toString() + " to enter that area " + timeText +
			 * ". Type /acceptcharge to accept the charge and enter.");
			 */
		} else if (isPrivate) {
			// System.out.println("checkpoint 2");

			// the player is on allow list
			if (plugin.getCcFlags().getAllow(claimid, playerUUIDString).equalsIgnoreCase(playerUUIDString)) {
				// they may enter
				// System.out.println("checkpoint 2.1");
				return true;
			}

			/*
			 * if(player != null) { player.sendMessage(ChatColor.RED + "This is a private claim!"); }
			 */
		}

		// System.out.println("checkpoint 3");
		return false;
	}

	/**
	 * Checks if location is in a GP Claim or not
	 *
	 * @param loc
	 * @return
	 */
	public Boolean check(Location loc) {
		try {

			// check if Grief Prevention is enabled in this world
			//if (plugin.ccGP.claimsEnabledForWorld(loc.getWorld())) {

				// get GP claim
				if (getGPClaim(loc) != null) {
					return true;
				}

			//}

		} catch (Exception e) {

			if (plugin.getCcGP() == null) {
				plugin.getLogger().warning("GriefPrevention instance is null! Please report this to Claim Commands.");
			} else if (plugin.getCcGP().dataStore == null) {
				plugin.getLogger().warning("GriefPrevention dataStore is null! Please report this to Claim Commands.");
			}

			plugin.getLogger().warning(e.getMessage());
		}

		return false;
	}

	/**
	 * Get a claims id value
	 *
	 * @param loc
	 * @return
	 */
	public Long getGPClaimId(Location loc) {

		// get GP claim
		me.ryanhamshire.GriefPrevention.Claim gpclaim = getGPClaim(loc);

		if (gpclaim != null) {

			// get GP claim id
			Long claimid = gpclaim.getID();
			return claimid;
		}

		// return -1L if no claim was found
		return -1L;
	}

	/**
	 * Get a claims owner value
	 *
	 * @param loc
	 * @return
	 */
	public UUID getOwner(Location loc) {

		// check if Grief Prevention is enabled in this world
		if (plugin.getCcGP().claimsEnabledForWorld(loc.getWorld())) {

			// get GP claim
			me.ryanhamshire.GriefPrevention.Claim gpclaim = getGPClaim(loc);
			if (gpclaim != null) {
				return gpclaim.ownerID;
			}
		}
		return null;
	}

	public boolean canExit(Player player, Location lastLocation) {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * Remove all flags for a claim
	 *
	 * @param claimid
	 */
	public void remove(long claimid) {
		plugin.getCcFlags().removeAllFlags(claimid);
	}
}
