/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.events;

import me.ryvix.claimcommands.functions.CheckPlayer;
import java.util.List;
import java.util.UUID;
import me.ryanhamshire.GriefPrevention.events.ClaimDeletedEvent;
import me.ryvix.claimcommands.ClaimCommands;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.projectiles.ProjectileSource;

public class CCEventHandler implements Listener {

	private final ClaimCommands plugin;

	/**
	 * Claim Commands Event Handler Constructor
	 *
	 * @param plugin
	 */
	public CCEventHandler(ClaimCommands plugin) {
		this.plugin = plugin;
	}

	/**
	 * Track newly loaded worlds
	 *
	 * @param event
	 */
	@EventHandler
	public void WorldLoad(WorldLoadEvent event) {

		World world = event.getWorld();
		
		if (world == null) {
			return;
		}
		
		plugin.getCcWorlds().add(world);
	}

	/**
	 * Stop tracking unloaded worlds
	 *
	 * @param event
	 */
	@EventHandler
	public void WorldUnload(WorldUnloadEvent event) {

		World world = event.getWorld();

		if (plugin.getCcWorlds().contains(world)) {
			plugin.getCcWorlds().remove(world);
		}
	}

	/**
	 * Remove flags for a claim if it gets deleted in Grief Prevention
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onClaimDeleted(ClaimDeletedEvent event) {
		me.ryanhamshire.GriefPrevention.Claim claim = event.getClaim();

		Long claimid;
		if (claim.parent != null) {
			claimid = claim.parent.getID();
		} else {
			claimid = claim.getID();
		}

		plugin.getCcClaim().remove(claimid);
	}

	/**
	 * Detect player bed enter
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerBedEnter(PlayerBedEnterEvent event) {
		Player player = event.getPlayer();
		Location location = event.getBed().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player empty bucket
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerEmptyBucket(PlayerBucketEmptyEvent event) {
		Player player = event.getPlayer();
		Location location = event.getBlockClicked().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player fill bucket
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerFillBucket(PlayerBucketFillEvent event) {
		Player player = event.getPlayer();
		Location location = event.getBlockClicked().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player commands
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		Location location = player.getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player interacting with entities
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		Location location = event.getRightClicked().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player interact
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();

		Location location = null;
		Action action = event.getAction();

		if (action == Action.LEFT_CLICK_BLOCK || action == Action.RIGHT_CLICK_BLOCK || action == Action.PHYSICAL) {
			// if they clicked a block check where the block is
			Block clickedBlock = event.getClickedBlock();
			if (clickedBlock == null) {
				return;
			}
			location = clickedBlock.getLocation();

		} else if (action == Action.LEFT_CLICK_AIR || action == Action.RIGHT_CLICK_AIR) {
			// if they clicked air check where the player is looking
			location = player.getEyeLocation();
		}

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			// player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player move
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		final UUID playerUUID = player.getUniqueId();
		plugin.addCheckPlayer(playerUUID, new CheckPlayer(playerUUID, event.getTo()));
	}

	/**
	 * Detect player pickup item
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
		Location location = event.getItem().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			// player.sendMessage(ChatColor.RED + "You aren't allowed there!");
		}
	}

	/**
	 * Detect player drop item
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		Location location = event.getItemDrop().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			// player.sendMessage(ChatColor.RED + "You aren't allowed there!");
		}
	}

	/**
	 * Detect player shear
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerShearEntity(PlayerShearEntityEvent event) {
		Player player = event.getPlayer();
		Location location = event.getEntity().getLocation();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to do that!");
		}
	}

	/**
	 * Detect player teleport
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		Location location = event.getTo();

		// keep players out but let admins in claims
		if (!plugin.getCcClaim().canEnter(player.getUniqueId(), location) && !player.hasPermission("claimcommands.admin")) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "You aren't allowed to go there!");
		}
	}

	/**
	 * Detect player portal
	 *
	 * @param event
	 */
	/*
	 * @EventHandler(priority = EventPriority.LOWEST) public void onPlayerPortal(PlayerPortalEvent event) { Player player = event.getPlayer(); Location fromLocation = event.getFrom(); Location
	 * toLocation = event.getTo();
	 * 
	 * // check if the claim is protected Boolean isClaimFrom = plugin.ccClaim.check(player, fromLocation); Boolean isClaimTo = plugin.ccClaim.check(player, toLocation);
	 * 
	 * // keep players out but let admins in claims if ((isClaimFrom || isClaimTo) && (!plugin.ccClaim.canEnter(player, fromLocation) || !plugin.ccClaim.canEnter(player, toLocation)) &&
	 * !player.hasPermission("claimcommands.admin")) { event.setCancelled(true); player.sendMessage(ChatColor.RED + "You aren't allowed to go there!"); } }
	 */
	/**
	 * Detect portal create
	 *
	 * @param event
	 */
	/*
	 * @EventHandler(priority = EventPriority.LOWEST) public void onPortalCreate(PortalCreateEvent event) { reason = event.getReason(); //blocks = event.getBlocks();
	 * 
	 * // check if the claim is protected Boolean isClaimFrom = plugin.ccClaim.check(player, fromLocation); Boolean isClaimTo = plugin.ccClaim.check(player, toLocation);
	 * 
	 * // keep players out but let admins in claims if ((isClaimFrom || isClaimTo) && (!plugin.ccClaim.canEnter(player, fromLocation) || !plugin.ccClaim.canEnter(player, toLocation)) &&
	 * !player.hasPermission("claimcommands.admin")) { event.setCancelled(true); player.sendMessage(ChatColor.RED + "You aren't allowed to go there!"); } }
	 */
	/**
	 * Prevent PvP if PvP is disabled
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {

		// System.out.println("Damage: " + event.getDamage());
		// System.out.println("Cause: " + event.getCause().name());

		Entity victim = event.getEntity();

		// check if entity receiving damage is a player
		if (victim instanceof Player) {

			Entity damager = event.getDamager();

			// check if entity dealing damage is a player
			if (!(damager instanceof Player) && !(damager instanceof Projectile)) {
				return;
			}

			Long claimid = plugin.getCcClaim().getGPClaimId(victim.getLocation());

			// if player is in a claim
			if (claimid != -1L) {

				// if no pvp allowed
				if (!plugin.getCcFlags().getPvp(claimid)) {

					// stop projectiles
					if (damager instanceof Projectile) {
						damager.remove();
					}

					event.setCancelled(true);
				}
			}
		}

	}

	/**
	 * Prevent combuster if PvP is disabled
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onEntityCombustByEntityEvent(EntityCombustByEntityEvent event) {

		Entity victim = event.getEntity();

		// check if entity receiving damage is a player
		if (!(victim instanceof Player)) {
			return;
		}

		Entity combuster = event.getCombuster();

		// check if combuster is a projectile
		if (combuster instanceof Projectile) {
			
			Long claimid = plugin.getCcClaim().getGPClaimId(victim.getLocation());

			// if player is in a claim
			if (claimid != -1L) {

				// if no pvp allowed
				if (!plugin.getCcFlags().getPvp(claimid)) {
					combuster.remove();
					event.setCancelled(true);
				}
			}
		}

	}

	/**
	 * Prevent splash potions if PvP is disabled
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onPotionSplashEvent(PotionSplashEvent event) {

		List<LivingEntity> affectedEntities = (List<LivingEntity>) event.getAffectedEntities();
		ThrownPotion potion = event.getPotion();
		ProjectileSource shooter = potion.getShooter();

		// check if entity dealing damage is a player
		if (!(shooter instanceof Player)) {
			return;
		}

		// check all affected entities
		for (LivingEntity victim : affectedEntities) {

			// check if entity receiving damage is a player
			if (!(victim instanceof Player)) {
				continue;
			}
			
			Long claimid = plugin.getCcClaim().getGPClaimId(victim.getLocation());

			// if player is in a claim
			if (claimid != -1L) {

				// if no pvp allowed
				if (!plugin.getCcFlags().getPvp(claimid)) {

					// block potions except on the victim
					Player shooterPlayer = (Player) shooter;
					if (shooterPlayer.getEntityId() != victim.getEntityId()) {
						event.setIntensity(victim, -1.0);
					}
				}
			}
		}
	}

	/**
	 * Prevent animals or monsters if they are disabled
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onCreatureSpawn(CreatureSpawnEvent event) {

		Entity entity = event.getEntity();

		// if monster is in a claim
		if (plugin.getCcClaim().check(entity.getLocation())) {
			Long claimid = plugin.getCcClaim().getGPClaimId(entity.getLocation());

			// if player is in a claim
			if (claimid != -1L) {

				// only animals
				if (entity instanceof Animals) {

					// if no animals allowed
					if (!plugin.getCcFlags().getAnimals(claimid)) {

						// don't remove this mob if it's already set to be removed
						if (plugin.getEntityUUIDs().contains(entity.getUniqueId())) {
							return;
						}

						event.setCancelled(true);
					}
				}

				// only monsters
				else if (entity instanceof Monster) {

					// if no monsters allowed
					if (!plugin.getCcFlags().getMonsters(claimid)) {

						// don't remove this mob if it's already set to be removed
						if (plugin.getEntityUUIDs().contains(entity.getUniqueId())) {
							return;
						}

						event.setCancelled(true);
					}
				}
			}
		}
	}
}
