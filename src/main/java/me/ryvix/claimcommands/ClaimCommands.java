/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands;

import me.ryvix.claimcommands.events.CCEventHandler;
import me.ryvix.claimcommands.functions.CheckPlayer;
import me.ryvix.claimcommands.data.Flags;
import me.ryvix.claimcommands.tasks.MonstersTask;
import me.ryvix.claimcommands.tasks.AnimalsTask;
import me.ryvix.claimcommands.tasks.ClaimTask;
import me.ryvix.claimcommands.functions.SQLFunctions;
import me.ryvix.claimcommands.functions.FlagsFunctions;
import me.ryvix.claimcommands.functions.ClaimFunctions;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import javax.persistence.PersistenceException;
import me.ryanhamshire.GriefPrevention.Claim;
//import net.milkbowl.vault.economy.Economy;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import me.ryanhamshire.GriefPrevention.PlayerData;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
//import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class ClaimCommands extends JavaPlugin {

	// Initialize variables
	private Configuration ccConfig;
	private SQLFunctions ccSql;
	private FlagsFunctions ccFlags;
	private ClaimFunctions ccClaim;
//	public Economy econ;
	private ConcurrentHashMap<UUID, CheckPlayer> ccCheckPlayers;
	private List<UUID> ccEntityUUIDs;
	private BukkitTask ccClaimTask;
	private BukkitTask ccMonstersTask;
	private Set<World> ccWorlds;
	private String ccDbType;
	private GriefPrevention ccGP;
	private BukkitTask ccAnimalsTask;

	/**
	 * Runs when plugin is enabled
	 *
	 */
	@Override
	public void onEnable() {

		// check for GriefPrevention plugin
		if (this.getServer().getPluginManager().getPlugin("GriefPrevention") == null) {
			this.getLogger().severe("ClamControl requires the Grief Prevention plugin but it wasn't found, disabling ClamControl!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		// check for Vault plugin
		/*
		 * if (this.getServer().getPluginManager().getPlugin("Vault") == null) { this.getLogger().severe("ClamControl requires the Vault plugin but it wasn't found, disabling ClamControl!");
		 * getServer().getPluginManager().disablePlugin(this); return; }
		 */

		// setup Vault Economy, requires economy plugin
		/*
		 * if (!setupEconomy()) { this.getLogger().severe("ClamControl requires an economy plugin supported by Vault but one wasn't found, disabling ClamControl!");
		 * getServer().getPluginManager().disablePlugin(this); return; }
		 */

		// create config file
		try {
			File configFile = new File(getDataFolder(), "config.yml");
			if (!configFile.exists()) {
				getDataFolder().mkdir();
				this.getConfig().options().copyDefaults(true);
				this.getConfig().options().header("Claim Commands config file\n\nDatabase config is in bukkit.yml\nSee: https://github.com/ty2u/Claim-Commands/wiki/Database-Setup\n ");
				this.getConfig().options().copyHeader(false);
				this.saveConfig();
			}

		} catch (Exception e) {
			getLogger().log(Level.WARNING, "Error: {0}", e.getMessage());
		}

		// load config file
		loadConfig();

		// register events
		CCEventHandler ccEventHandler = new CCEventHandler(this);
		this.getServer().getPluginManager().registerEvents(ccEventHandler, this);
	}

	/**
	 * Setup Vault Economy
	 *
	 * @return
	 */
	/*
	 * private boolean setupEconomy() {
	 * 
	 * RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class); if (rsp == null) { return false; }
	 * 
	 * econ = rsp.getProvider();
	 * 
	 * return econ != null; }
	 */
	/**
	 * Runs when plugin is disabled
	 *
	 */
	@Override
	public void onDisable() {

		// stop tasks
		stopTasks();

		// null some variables
		setCcConfig(null);
		setCcSql(null);
		setCcFlags(null);
		setCcClaim(null);
//		econ = null;
		setCcCheckPlayers(null);
		setCcClaimTask(null);
		setCcMonstersTask(null);
		setCcAnimalsTask(null);
		setCcWorlds(null);
		setCcDbType(null);
		setCcGP(null);
	}

	/**
	 * Load config values
	 *
	 */
	public void loadConfig() {

		// get config file
		FileConfiguration getConfig = getConfig();
		File configFile = new File(this.getDataFolder() + "/config.yml");
		setCcConfig(YamlConfiguration.loadConfiguration(configFile));

		// add defaults
		if (!ccConfig.contains("config.default_flags.entrymsg")) {
			getConfig().addDefault("config.default.entrymsg", "");
		}
		if (!ccConfig.contains("config.default_flags.exitmsg")) {
			getConfig().addDefault("config.default.exitmsg", "");
		}
		if (!ccConfig.contains("config.default_flags.monsters")) {
			getConfig().addDefault("config.default.monsters", "true");
		}
		if (!ccConfig.contains("config.default_flags.animals")) {
			getConfig().addDefault("config.default_flags.animals", "true");
		}
		if (!ccConfig.contains("config.default_flags.private")) {
			getConfig().addDefault("config.default.private", "false");
		}
		if (!ccConfig.contains("config.default_flags.pvp")) {
			getConfig().addDefault("config.default.pvp", "true");
		}
		if (!ccConfig.contains("config.check_claims_ticks")) {
			getConfig().addDefault("config.check_claims_ticks", 71);
		}
		if (!ccConfig.contains("config.remove_monsters_ticks")) {
			getConfig().addDefault("config.remove_monsters_ticks", 60);
		}
		if (!ccConfig.contains("config.remove_animals_ticks")) {
			getConfig().addDefault("config.remove_animals_ticks", 82);
		}
		getConfig.options().copyDefaults(true);

		// add header
		getConfig().options().header("Claim Commands config file\n\nDatabase config is in bukkit.yml\nSee: https://github.com/ty2u/Claim-Commands/wiki/Database-Setup\n ");
		getConfig().options().copyHeader(false);

		// save file
		saveConfig();

		// make sure an SQL table is created
		setupDatabase();

		// set values
		setVariables();

		// start tasks
		startTasks();
	}

	/**
	 * Set config variables
	 *
	 */
	public void setVariables() {

		setCcGP(GriefPrevention.instance);

		setCcWorlds(new HashSet<World>());
		for (World world : getServer().getWorlds()) {
			if (getCcGP().claimsEnabledForWorld(world)) {
				getCcWorlds().add(world);
			}
		}

		// SQL
		setCcSql(new SQLFunctions(this));

		// Flags functions
		setCcFlags(new FlagsFunctions(this, getCcSql()));

		// Claim functions
		setCcClaim(new ClaimFunctions(this));

		// initialize ccCheckPlayers
		setCcCheckPlayers(new ConcurrentHashMap<UUID, CheckPlayer>());

		// initialize removedEntities
		setEntityUUIDs(new ArrayList<UUID>());
	}

	/**
	 * Start runnable tasks
	 */
	private void startTasks() {
		// Claim task
		long claimTaskTime = Long.valueOf(getCcConfig().getInt("config.check_claims_ticks"));
		if (claimTaskTime > 0) {
			this.getLogger().log(Level.INFO, "Starting claim check task. check_claims_ticks: {0}", getCcConfig().getInt("config.check_claims_ticks"));
			// ccClaimTask = new ClaimTask(this).runTaskTimerAsynchronously(this, 0, claimTaskTime);
			setCcClaimTask(new ClaimTask(this).runTaskTimer(this, 0, claimTaskTime));
		}

		// Monsters task
		long monstersTaskTime = Long.valueOf(getCcConfig().getInt("config.remove_monsters_ticks"));
		if (monstersTaskTime > 0) {
			this.getLogger().log(Level.INFO, "Starting monster removal task. remove_monsters_ticks: {0}", getCcConfig().getInt("config.remove_monsters_ticks"));
			setCcMonstersTask(new MonstersTask(this).runTaskTimer(this, 0, monstersTaskTime));
		}

		// Animals task
		long animalsTaskTime = Long.valueOf(getCcConfig().getInt("config.remove_animals_ticks"));
		if (animalsTaskTime > 0) {
			this.getLogger().log(Level.INFO, "Starting animal removal task. remove_animals_ticks: {0}", getCcConfig().getInt("config.remove_animals_ticks"));
			setCcAnimalsTask(new AnimalsTask(this).runTaskTimer(this, 0, animalsTaskTime));
		}
	}

	/**
	 * Stop runnable tasks
	 */
	private void stopTasks() {
		// Claim task
		long claimTaskTime = Long.valueOf(getCcConfig().getInt("config.check_claims_ticks"));
		if (claimTaskTime > 0) {
			getCcClaimTask().cancel();
		}

		// Monsters task
		long monstersTaskTime = Long.valueOf(getCcConfig().getInt("config.remove_monsters_ticks"));
		if (monstersTaskTime > 0) {
			getCcMonstersTask().cancel();
		}

		// Animals task
		long animalsTaskTime = Long.valueOf(getCcConfig().getInt("config.remove_animals_ticks"));
		if (animalsTaskTime > 0) {
			getCcAnimalsTask().cancel();
		}
	}

	/**
	 * Get players to check for claim permissions
	 *
	 * @return
	 */
	public ConcurrentHashMap<UUID, CheckPlayer> getCheckPlayers() {
		return getCcCheckPlayers();
	}

	/**
	 * Get a player to check for claim permissions
	 *
	 * @param playerUUID
	 * @return
	 */
	public CheckPlayer getCheckPlayer(UUID playerUUID) {
		return getCcCheckPlayers().get(playerUUID);
	}

	/**
	 * Add a player to check for claim permissions
	 *
	 * @param playerUUID
	 * @param checkPlayer
	 */
	public void addCheckPlayer(UUID playerUUID, CheckPlayer checkPlayer) {
		// only add player if they don't already exist
		if (getCheckPlayer(playerUUID) == null) {
			this.getCcCheckPlayers().putIfAbsent(playerUUID, checkPlayer);
		}
	}

	/**
	 * Remove a player to check for claim permissions
	 *
	 * @param playerUUID
	 */
	public void removeCheckPlayer(UUID playerUUID) {
		this.getCcCheckPlayers().remove(playerUUID);
	}

	/**
	 * Get removed entities
	 *
	 * @return
	 */
	public List<UUID> getEntityUUIDs() {
		return getCcEntityUUIDs();
	}

	/**
	 * Set removed entities
	 *
	 * @param removedEntities
	 */
	public void setEntityUUIDs(List<UUID> removedEntities) {
		this.setCcEntityUUIDs(removedEntities);
	}

	/**
	 * Add removed entity
	 *
	 * @param id
	 */
	public void addEntityUUID(UUID id) {
		this.getCcEntityUUIDs().add(id);
	}

	/**
	 * Remove entity
	 *
	 * @param id
	 */
	public void removeEntityUUID(UUID id) {
		this.getCcEntityUUIDs().remove(id);
	}

	/**
	 * Commands
	 *
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		// /claimcommands (alias /cc)
		if (cmd.getName().equalsIgnoreCase("claimcommands")) {

			// help
			boolean showHelp = false;
			if (args.length == 0) {
				showHelp = true;
			} else if (args[0] != null && args[0].equalsIgnoreCase("help")) {
				showHelp = true;
			}
			if (showHelp) {
				sender.sendMessage(ChatColor.BLACK + "                     -+[[" + ChatColor.DARK_AQUA + " Claim Commands Help " + ChatColor.BLACK + "]]+-");
				sender.sendMessage(ChatColor.GRAY + "Parameters in <angle brackets> are required.");
				sender.sendMessage(ChatColor.GRAY + "Parameters in [square brackets] may be optional.");
				sender.sendMessage(ChatColor.YELLOW + "/claimcommands flags" + ChatColor.WHITE + " See help on the flags available to use.");
				if (sender.hasPermission("claimcommands.add")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands add <flag> <value>" + ChatColor.WHITE + " Add a value to a flag.");
				}
				if (sender.hasPermission("claimcommands.remove")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands remove <flag> [value]" + ChatColor.WHITE + " Remove flag or a value from a flag.");
				}
				if (sender.hasPermission("claimcommands.transfer") || sender.hasPermission("claimcommands.transfer.player")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands transfer <player>" + ChatColor.WHITE + " Transfer a claim to another player.");
				}
				if (sender.hasPermission("claimcommands.list")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands list [flagname]" + ChatColor.WHITE + " List the flags in the claim you are currently in or if a flag is given it will list the values for the given flag.");
				}
				if (sender.hasPermission("claimcommands.flags.allow")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands allow <player>" + ChatColor.WHITE + " A shortcut to add a player to the allow list.");
				}
				if (sender.hasPermission("claimcommands.flags.deny")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands deny <player>" + ChatColor.WHITE + " A shortcut to add a player to the deny list.");
				}
				if (sender.hasPermission("claimcommands.flags.entrymsg")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands entrymsg <value>" + ChatColor.WHITE + " A shortcut to set the entrymsg flag.");
				}
				if (sender.hasPermission("claimcommands.flags.exitmsg")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands exitmsg <value>" + ChatColor.WHITE + " A shortcut to set the exitmsg flag.");
				}
				if (sender.hasPermission("claimcommands.flags.monsters")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands monsters" + ChatColor.WHITE + " A shortcut to toggle the monsters flag.");
				}
				if (sender.hasPermission("claimcommands.flags.animals")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands animals" + ChatColor.WHITE + " A shortcut to toggle the animals flag.");
				}
				if (sender.hasPermission("claimcommands.flags.private")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands private" + ChatColor.WHITE + " A shortcut to toggle the private flag.");
				}
				if (sender.hasPermission("claimcommands.flags.pvp")) {
					sender.sendMessage(ChatColor.YELLOW + "/claimcommands pvp" + ChatColor.WHITE + " A shortcut to toggle the pvp flag.");
				}
				sender.sendMessage(ChatColor.BLACK + "               -+[[]][[]][[]][[]][[]][[]][[]][[]][[]][[]]+-");
				return true;
			}

			if (args[0].equalsIgnoreCase("flags")) {
				sender.sendMessage(ChatColor.BLACK + "                            -+[[" + ChatColor.DARK_AQUA + " Flags " + ChatColor.BLACK + "]]+-");
				/*
				 * if (sender.hasPermission("claimcommands.ccFlags.animals")) { sender.sendMessage(ChatColor.YELLOW + "animals" + ChatColor.WHITE +
				 * " Any animals that enter the ccClaim without this flag will disappear. Values: true, false"); }
				 */
				if (sender.hasPermission("claimcommands.flags.monsters")) {
					sender.sendMessage(ChatColor.YELLOW + "monsters" + ChatColor.WHITE + " Any monsters that enter the claim without this flag will disappear. Values: true, false. Default: true");
				}
				if (sender.hasPermission("claimcommands.flags.animals")) {
					sender.sendMessage(ChatColor.YELLOW + "animals" + ChatColor.WHITE + " Any animals that enter the claim without this flag will disappear. Values: true, false. Default: true");
				}
				if (sender.hasPermission("claimcommands.flags.pvp")) {
					sender.sendMessage(ChatColor.YELLOW + "pvp" + ChatColor.WHITE + " Adding this flag will enable PvP in the claim. Values: true, false. Default: false");
				}
				if (sender.hasPermission("claimcommands.flags.entrymsg")) {
					sender.sendMessage(ChatColor.YELLOW + "entrymsg" + ChatColor.WHITE + " Set an entry message. Example: /claimcommands add entrymsg Your entry message.");
				}
				if (sender.hasPermission("claimcommands.flags.exitmsg")) {
					sender.sendMessage(ChatColor.YELLOW + "exitmsg" + ChatColor.WHITE + " Set an exit message. Example: /claimcommands add exitmsg Your exit message.");
				}
				/*
				 * if (sender.hasPermission("claimcommands.ccFlags.charge")) { sender.sendMessage(ChatColor.YELLOW + "charge" + ChatColor.WHITE +
				 * " Charge players to enter. Example: /claimcommands add charge 5"); } if (sender.hasPermission("claimcommands.ccFlags.time")) { sender.sendMessage(ChatColor.YELLOW + "time" +
				 * ChatColor.WHITE + " Set a time for the charge flag. After it expires they must pay again. Example: /claimcommands add time 1w3d3h7m"); } if
				 * (sender.hasPermission("claimcommands.ccFlags.trust")) { sender.sendMessage(ChatColor.YELLOW + "trust" + ChatColor.WHITE +
				 * " Set the trust level they receive when they pay. Values: none, trust, container, access, permission"); }
				 */
				if (sender.hasPermission("claimcommands.flags.allow")) {
					sender.sendMessage(ChatColor.YELLOW + "allow" + ChatColor.WHITE + " Allow a player to enter a private claim, bypassing any charges. Example: /claimcommands add allow PlayerName");
				}
				if (sender.hasPermission("claimcommands.flags.deny")) {
					sender.sendMessage(ChatColor.YELLOW + "deny" + ChatColor.WHITE + " Deny a player to enter a public claim. Example: /claimcommands add deny PlayerName");
				}
				if (sender.hasPermission("claimcommands.flags.private")) {
					sender.sendMessage(ChatColor.YELLOW + "private" + ChatColor.WHITE + " Set a claim as private, preventing anyone from entering unless you allow them. Note that you can use /claimcommands private to toggle this value. Values: true, false. Default: false");
				}
				/*
				 * if (sender.hasPermission("claimcommands.ccFlags.box")) { sender.sendMessage(ChatColor.YELLOW + "box" + ChatColor.WHITE +
				 * " Set a ccClaim as a box, preventing anyone from exiting unless you allow them. Values: true, false"); }
				 */
				sender.sendMessage(ChatColor.BLACK + "               -+[[]][[]][[]][[]][[]][[]][[]][[]][[]][[]]+-");
				return true;
			}

			// reload the plugin
			if (args[0].equalsIgnoreCase("reload") && sender.hasPermission("claimcommands.admin")) {
				getServer().getPluginManager().disablePlugin(this);
				getServer().getPluginManager().enablePlugin(this);
				getLogger().info("Reloaded");
				if ((sender instanceof Player)) {
					sender.sendMessage(ChatColor.GREEN + "Claim Commands has been reloaded");
				}
				return true;
			}

			// player commands
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Location loc = player.getLocation();

				// check if the claim is protected
				boolean isClaim = getCcClaim().check(loc);
				if (!isClaim) {
					player.sendMessage(ChatColor.RED + "You are not in a claim.");
					return true;
				}

				long claimid = getCcClaim().getGPClaimId(loc);

				if (args.length == 1) {
					// /claimcommands args[0]

					/**
					 * Convert a claim to an admin claim.
					 *
					 * @usage /claimcommands convert
					 * @permission claimcommands.convert
					 */
					if (args[0].equalsIgnoreCase("convert")) {
						if (player.hasPermission("claimcommands.list")) {

							// check additional permission
							if (!player.hasPermission("claimcommands.convert")) {
								player.sendMessage(ChatColor.RED + "You don't have permission to do that");
								return true;
							}

							Claim claim = getCcClaim().getGPClaim(loc);
							if (claim != null) {
								// change ownerhsip
								if (claim.parent != null) {
									player.sendMessage(ChatColor.RED + "Only top level claims (not subdivisions) may be converted.  Stand outside of the subdivision and try again.");
									return true;
								}

								//determine current claim owner
								PlayerData ownerData;
								if (!claim.isAdminClaim()) {
									ownerData = getCcGP().dataStore.getPlayerData(claim.ownerID);
								} else {
									player.sendMessage(ChatColor.RED + "This claim is already an admin claim.");
									return true;
								}

								claim.ownerID = null;
								getCcGP().dataStore.saveClaim(claim);

								//adjust blocks and other records
								ownerData.getClaims().remove(claim);
								//ownerData.bonusClaimBlocks += claim.getArea();
								getCcGP().dataStore.savePlayerData(claim.ownerID, ownerData);

								// confirm
								player.sendMessage(ChatColor.GREEN + "Claim converted to an admin claim.");
								this.getLogger().log(Level.INFO, "{0} converted a claim at {1} to an admin claim.", new Object[]{player.getName(), GriefPrevention.getfriendlyLocationString(claim.getLesserBoundaryCorner())});
							}

							return true;
						}

					}

					/**
					 * Player listing flags in a claim
					 *
					 * @usage /claimcommands list
					 * @permission claimcommands.list
					 */
					if (args[0].equalsIgnoreCase("list")) {
						if (player.hasPermission("claimcommands.list")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.list.others")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							String output = getCcFlags().list(loc);
							player.sendMessage(output);

							return true;
						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

					/**
					 * Toggles
					 */
					// toggle private flag
					if (args[0].equalsIgnoreCase("private") && player.hasPermission("claimcommands.flags.private")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().getPrivate(claimid)) {
							getCcFlags().removeFlag(claimid, "private");
							getCcFlags().setPrivate(claimid, "false");
							player.sendMessage(ChatColor.GREEN + "This claim was set as public.");
						} else {
							getCcFlags().removeFlag(claimid, "private");
							getCcFlags().setPrivate(claimid, "true");
							player.sendMessage(ChatColor.GREEN + "This claim was set as private.");
						}
						return true;
					}

					// toggle pvp flag
					if (args[0].equalsIgnoreCase("pvp") && player.hasPermission("claimcommands.flags.pvp")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().getPvp(claimid)) {
							getCcFlags().removeFlag(claimid, "pvp");
							getCcFlags().setPvp(claimid, "false");
							player.sendMessage(ChatColor.GREEN + "This claim was set as Non-PvP.");
						} else {
							getCcFlags().removeFlag(claimid, "pvp");
							getCcFlags().setPvp(claimid, "true");
							player.sendMessage(ChatColor.GREEN + "This claim was set as PvP.");
						}
						return true;
					}

					// toggle monsters flag
					if (args[0].equalsIgnoreCase("monsters") && player.hasPermission("claimcommands.flags.monsters")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().getMonsters(claimid)) {
							getCcFlags().removeFlag(claimid, "monsters");
							getCcFlags().setMonsters(claimid, "false");
							player.sendMessage(ChatColor.GREEN + "This claim was set as a monster free zone.");
						} else {
							getCcFlags().removeFlag(claimid, "monsters");
							getCcFlags().setMonsters(claimid, "true");
							player.sendMessage(ChatColor.GREEN + "This claim was set to allow monsters.");
						}
						return true;
					}

					// toggle animals flag
					if (args[0].equalsIgnoreCase("animals") && player.hasPermission("claimcommands.flags.animals")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().getAnimals(claimid)) {
							getCcFlags().removeFlag(claimid, "animals");
							getCcFlags().setAnimals(claimid, "false");
							player.sendMessage(ChatColor.GREEN + "This claim was set as an animal free zone.");
						} else {
							getCcFlags().removeFlag(claimid, "animals");
							getCcFlags().setAnimals(claimid, "true");
							player.sendMessage(ChatColor.GREEN + "This claim was set to allow animals.");
						}
						return true;
					}

				} else if (args.length == 2) {
					// /claimcommands args[0] args[1]

					/**
					 * Transfer a claim to a new owner
					 *
					 * @usage /claimcommands transfer <player>
					 * @permission claimcommands.transfer
					 * @permission claimcommands.transfer.player
					 */
					if (args[0].equalsIgnoreCase("transfer")) {
						// check additional permission
						if (!player.hasPermission("claimcommands.transfer.player")) {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that");
							return true;
						}

						OfflinePlayer targetPlayer = getCcGP().resolvePlayerByName(args[1]);
						if (targetPlayer == null) {
							player.sendMessage(ChatColor.RED + "Target player not found.");
							return true;
						}

						me.ryanhamshire.GriefPrevention.Claim claim = getCcGP().dataStore.getClaimAt(loc, true, null);
						if (claim != null) {

							// if player doesn't owns the claim they are in and doesn't have transfer permission they can't do it
							if (claim.ownerID != player.getUniqueId() && !player.hasPermission("claimcommands.transfer")) {
								player.sendMessage(ChatColor.RED + "You don't have permission to do that");
								return true;
							}

							// change ownerhsip
							try {
								getCcGP().dataStore.changeClaimOwner(claim, targetPlayer.getUniqueId());
							} catch (Exception e) {
								player.sendMessage(ChatColor.RED + "Only top level claims (not subdivisions) may be transferred.  Stand outside of the subdivision and try again.");
								return true;
							}

							// confirm
							player.sendMessage(ChatColor.GREEN + "Claim transferred.");
							this.getLogger().log(Level.INFO, "{0} transferred a claim at {1} to " + targetPlayer.getName() + ".", new Object[]{player.getName(), GriefPrevention.getfriendlyLocationString(claim.getLesserBoundaryCorner())});

						}

						return true;
					}

					/**
					 * Player trying to add a new flag but not enough args
					 *
					 * @usage /claimcommands add <flag>
					 * @permission claimcommands.add
					 */
					if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("set")) {
						if (player.hasPermission("claimcommands.add")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							player.sendMessage(ChatColor.RED + "Usage: /claimcommands add <flag> <value>");
							return true;
						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

					// shortcut for allow flag
					if (args[0].equalsIgnoreCase("allow") && player.hasPermission("claimcommands.flags.allow")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().hasFlag(claimid, "allow", args[1])) {
							player.sendMessage(ChatColor.RED + "That player is already allowed.");
						} else {
							getCcFlags().setAllow(claimid, args[1]);
							player.sendMessage(ChatColor.GREEN + args[1] + " was added to the allow list.");
						}
						return true;
					}

					// shortcut for deny flag
					if (args[0].equalsIgnoreCase("deny") && player.hasPermission("claimcommands.flags.deny")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						if (getCcFlags().hasFlag(claimid, "deny", args[1])) {
							player.sendMessage(ChatColor.RED + "That player is already denied.");
						} else {
							getCcFlags().setDeny(claimid, args[1]);
							player.sendMessage(ChatColor.GREEN + args[1] + " was added to the deny list.");
						}
						return true;
					}

					/**
					 * Player trying to remove a flag but not enough args
					 *
					 * @usage /claimcommands remove <flag>
					 * @permission claimcommands.remove
					 */
					if (args[0].equalsIgnoreCase("remove")) {
						if (player.hasPermission("claimcommands.remove")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							// remove monsters flag
							if (args[1].equalsIgnoreCase("monsters")) {
								getCcFlags().removeFlag(claimid, "monsters");
								player.sendMessage(ChatColor.RED + "Monsters flag removed!");
								return true;
							}

							// remove animals flag
							if (args[1].equalsIgnoreCase("animals")) {
								getCcFlags().removeFlag(claimid, "animals");
								player.sendMessage(ChatColor.RED + "Animals flag removed!");
								return true;
							}

							// remove pvp flag
							if (args[1].equalsIgnoreCase("pvp")) {
								getCcFlags().removeFlag(claimid, "pvp");
								player.sendMessage(ChatColor.RED + "PvP flag removed!");
								return true;
							}

							// remove entry message flag
							if (args[1].equalsIgnoreCase("entrymsg")) {
								getCcFlags().removeFlag(claimid, "entrymsg");
								player.sendMessage(ChatColor.RED + "Entry message removed!");
								return true;
							}

							// remove exit message flag
							if (args[1].equalsIgnoreCase("exitmsg")) {
								getCcFlags().removeFlag(claimid, "exitmsg");
								player.sendMessage(ChatColor.RED + "Entry message removed!");
								return true;
							}

							// remove charge flag
							if (args[1].equalsIgnoreCase("charge")) {
								getCcFlags().removeFlag(claimid, "charge");
								player.sendMessage(ChatColor.RED + "Charge flag removed!");
								return true;
							}

							// remove time flag
							if (args[1].equalsIgnoreCase("time")) {
								getCcFlags().removeFlag(claimid, "time");
								player.sendMessage(ChatColor.RED + "Time flag removed!");
								return true;
							}

							// remove trust flag
							if (args[1].equalsIgnoreCase("trust")) {
								getCcFlags().removeFlag(claimid, "trust");
								player.sendMessage(ChatColor.RED + "Trust flag removed!");
								return true;
							}

							// remove allow flag
							if (args[1].equalsIgnoreCase("allow")) {
								getCcFlags().removeFlag(claimid, "allow");
								player.sendMessage(ChatColor.RED + "Allow flag removed!");
								return true;
							}

							// remove deny flag
							if (args[1].equalsIgnoreCase("deny")) {
								getCcFlags().removeFlag(claimid, "deny");
								player.sendMessage(ChatColor.RED + "Deny flag removed!");
								return true;
							}

							// remove private flag
							if (args[1].equalsIgnoreCase("private")) {
								getCcFlags().removeFlag(claimid, "private");
								player.sendMessage(ChatColor.RED + "Private flag removed!");
								return true;
							}

							// remove box flag
							if (args[1].equalsIgnoreCase("box")) {
								getCcFlags().removeFlag(claimid, "box");
								player.sendMessage(ChatColor.RED + "Box flag removed!");
								return true;
							}

						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

					/**
					 * Player listing flags in a claim
					 *
					 * @usage /claimcommands list <flag>
					 * @permission claimcommands.list
					 */
					if (args[0].equalsIgnoreCase("list")) {
						if (player.hasPermission("claimcommands.list")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.list.others")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							// validate flag
							if (!ccFlags.valid(args[1])) {
								player.sendMessage(ChatColor.RED + "Valid flags: " + FlagsFunctions.validFlags);
								return true;
							} else {

								String output = getCcFlags().list(loc, args[1]);
								player.sendMessage(output);

								return true;
							}
						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

				} else if (args.length == 3) {
					// /claimcommands args[0] args[1] args[2]

					/**
					 * Player adding a new flag
					 *
					 * @usage /claimcommands add <flag> <value>
					 * @permission claimcommands.add
					 */
					if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("set")) {
						if (player.hasPermission("claimcommands.add")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							// validate flag
							if (!ccFlags.valid(args[1])) {
								player.sendMessage(ChatColor.RED + "Valid flags: " + FlagsFunctions.validFlags);
								return true;
							} else // there can be multiple allow and deny flags
							 if (args[1].equalsIgnoreCase("allow") || args[1].equalsIgnoreCase("deny")) {

									// check if claim has the flag and value
									if (getCcFlags().hasFlag(claimid, args[1], args[2])) {
										player.sendMessage(ChatColor.RED + args[1] + " already has " + args[2] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("allow") && player.hasPermission("claimcommands.flags.allow")) {
										// allow someone into a private claim
										getCcFlags().setAllow(claimid, args[2]);
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("deny") && player.hasPermission("claimcommands.flags.deny")) {
										// deny someone from enterying a public claim
										getCcFlags().setDeny(claimid, args[2]);
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									}

								} else {
									
									// check if claim has the flag
									if (getCcFlags().hasFlag(claimid, args[1])) {
										player.sendMessage(ChatColor.RED + args[1] + " already has " + args[2] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("monsters") && player.hasPermission("claimcommands.flags.monsters")) {
										// allow monsters
										getCcFlags().setMonsters(claimid, "true");
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("animals") && player.hasPermission("claimcommands.flags.animals")) {
										// allow animals
										getCcFlags().setAnimals(claimid, "true");
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("pvp") && player.hasPermission("claimcommands.flags.pvp")) {
										// allow pvp
										getCcFlags().setPvp(claimid, "true");
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("charge") && player.hasPermission("claimcommands.flags.charge")) {
										// set charge
										getCcFlags().setCharge(claimid, args[2]);
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("time") && player.hasPermission("claimcommands.flags.time")) {
										// set time
										getCcFlags().setTime(claimid, args[2]);
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("entrymsg") && player.hasPermission("claimcommands.flags.entrymsg")) {
										// set entry message
										String entryMsg = StringUtils.join(args, " ", 2, args.length);
										getCcFlags().setEntryMsg(claimid, entryMsg);
										player.sendMessage(ChatColor.GREEN + "Entry message set to: " + ChatColor.RESET + ChatColor.translateAlternateColorCodes("&".charAt(0), entryMsg));
										return true;
									} else if (args[1].equalsIgnoreCase("exitmsg") && player.hasPermission("claimcommands.flags.exitmsg")) {
										// set exit message
										String exitMsg = StringUtils.join(args, " ", 2, args.length);
										getCcFlags().setExitMsg(claimid, exitMsg);
										player.sendMessage(ChatColor.GREEN + "Exit message set to: " + ChatColor.RESET + ChatColor.translateAlternateColorCodes("&".charAt(0), exitMsg));
										return true;
									} else if (args[1].equalsIgnoreCase("private") && player.hasPermission("claimcommands.flags.private")) {
										// set private
										getCcFlags().setPrivate(claimid, "true");
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									} else if (args[1].equalsIgnoreCase("box") && player.hasPermission("claimcommands.flags.box")) {
										// set as box
										getCcFlags().setBox(claimid, "true");
										player.sendMessage(ChatColor.GREEN + args[2] + " added to " + args[1] + "!");
										return true;
									}
								}
						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

					/**
					 * Player removing a flag
					 *
					 * @usage /claimcommands remove <flag> <value>
					 * @permission claimcommands.flags.remove
					 */
					if (args[0].equalsIgnoreCase("remove")) {
						if (player.hasPermission("claimcommands.remove")) {

							// check if player owns the claim they are in
							UUID owner = getCcClaim().getOwner(loc);
							if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
								player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
								return true;
							}

							// check if claim has the flag
							if (!ccFlags.hasFlag(claimid, args[1], args[2])) {
								player.sendMessage(ChatColor.RED + args[2] + " does not exist in " + args[1]);
								return true;
							}

							getCcFlags().removeFlag(claimid, args[1], args[2]);

							player.sendMessage(ChatColor.GREEN + args[2] + " removed from " + args[1]);
							return true;

						} else {
							player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
							return true;
						}
					}

				}

				if (args.length >= 2) {
					// /claimcommands args[0] args[1]...

					// set entry message flag
					if (args[0].equalsIgnoreCase("entrymsg") && player.hasPermission("claimcommands.flags.entrymsg")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						getCcFlags().removeFlag(claimid, "entrymsg");
						String entryMsg = StringUtils.join(args, " ", 1, args.length);
						getCcFlags().setEntryMsg(claimid, entryMsg);
						player.sendMessage(ChatColor.GREEN + "Entry message set to: " + ChatColor.RESET + ChatColor.translateAlternateColorCodes("&".charAt(0), entryMsg));
						return true;
					}

					// set exit message flag
					if (args[0].equalsIgnoreCase("exitmsg") && player.hasPermission("claimcommands.flags.exitmsg")) {

						// check if player owns the claim they are in
						UUID owner = getCcClaim().getOwner(loc);
						if (!owner.toString().equalsIgnoreCase(player.getUniqueId().toString()) && !player.hasPermission("claimcommands.admin")) {
							player.sendMessage(ChatColor.RED + "The owner of this claim is " + owner + ".");
							return true;
						}

						getCcFlags().removeFlag(claimid, "exitmsg");
						String exitMsg = StringUtils.join(args, " ", 1, args.length);
						getCcFlags().setExitMsg(claimid, exitMsg);
						player.sendMessage(ChatColor.GREEN + "Exit message set to: " + ChatColor.RESET + ChatColor.translateAlternateColorCodes("&".charAt(0), exitMsg));
						return true;
					}

				}

			} else {
				// console commands
			}

		}

		sender.sendMessage(ChatColor.RED + "You have entered incorrect arguements! Type /claimcommands for help.");
		return true;
	}

	/**
	 * @return the config
	 */
	public Configuration getCcConfig() {
		return ccConfig;
	}

	/**
	 * @param ccConfig the config to set
	 */
	public void setCcConfig(Configuration ccConfig) {
		this.ccConfig = ccConfig;
	}

	/**
	 * @return the ccSql
	 */
	public SQLFunctions getCcSql() {
		return ccSql;
	}

	/**
	 * @param ccSql the ccSql to set
	 */
	public void setCcSql(SQLFunctions ccSql) {
		this.ccSql = ccSql;
	}

	/**
	 * @return the ccFlags
	 */
	public FlagsFunctions getCcFlags() {
		return ccFlags;
	}

	/**
	 * @param ccFlags the ccFlags to set
	 */
	public void setCcFlags(FlagsFunctions ccFlags) {
		this.ccFlags = ccFlags;
	}

	/**
	 * @return the ccClaim
	 */
	public ClaimFunctions getCcClaim() {
		return ccClaim;
	}

	/**
	 * @param ccClaim the ccClaim to set
	 */
	public void setCcClaim(ClaimFunctions ccClaim) {
		this.ccClaim = ccClaim;
	}

	/**
	 * @return the ccCheckPlayers
	 */
	public ConcurrentHashMap<UUID, CheckPlayer> getCcCheckPlayers() {
		return ccCheckPlayers;
	}

	/**
	 * @param ccCheckPlayers the ccCheckPlayers to set
	 */
	public void setCcCheckPlayers(ConcurrentHashMap<UUID, CheckPlayer> ccCheckPlayers) {
		this.ccCheckPlayers = ccCheckPlayers;
	}

	/**
	 * @return the ccEntityUUIDs
	 */
	public List<UUID> getCcEntityUUIDs() {
		return ccEntityUUIDs;
	}

	/**
	 * @param ccEntityUUIDs the ccEntityUUIDs to set
	 */
	public void setCcEntityUUIDs(List<UUID> ccEntityUUIDs) {
		this.ccEntityUUIDs = ccEntityUUIDs;
	}

	/**
	 * @return the ccClaimTask
	 */
	public BukkitTask getCcClaimTask() {
		return ccClaimTask;
	}

	/**
	 * @param ccClaimTask the ccClaimTask to set
	 */
	public void setCcClaimTask(BukkitTask ccClaimTask) {
		this.ccClaimTask = ccClaimTask;
	}

	/**
	 * @return the ccMonstersTask
	 */
	public BukkitTask getCcMonstersTask() {
		return ccMonstersTask;
	}

	/**
	 * @param ccMonstersTask the ccMonstersTask to set
	 */
	public void setCcMonstersTask(BukkitTask ccMonstersTask) {
		this.ccMonstersTask = ccMonstersTask;
	}

	/**
	 * @return the ccWorlds
	 */
	public Set<World> getCcWorlds() {
		return ccWorlds;
	}

	/**
	 * @param ccWorlds the ccWorlds to set
	 */
	public void setCcWorlds(Set<World> ccWorlds) {
		this.ccWorlds = ccWorlds;
	}

	/**
	 * @return the ccDbType
	 */
	public String getCcDbType() {
		return ccDbType;
	}

	/**
	 * @param ccDbType the ccDbType to set
	 */
	public void setCcDbType(String ccDbType) {
		this.ccDbType = ccDbType;
	}

	/**
	 * @return the ccGP
	 */
	public GriefPrevention getCcGP() {
		return ccGP;
	}

	/**
	 * @param ccGP the ccGP to set
	 */
	public void setCcGP(GriefPrevention ccGP) {
		this.ccGP = ccGP;
	}

	/**
	 * @return the ccAnimalsTask
	 */
	public BukkitTask getCcAnimalsTask() {
		return ccAnimalsTask;
	}

	/**
	 * @param ccAnimalsTask the ccAnimalsTask to set
	 */
	public void setCcAnimalsTask(BukkitTask ccAnimalsTask) {
		this.ccAnimalsTask = ccAnimalsTask;
	}

	@Override
	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> list = new ArrayList<>();
		list.add(Flags.class);
		return list;
	}

	private void setupDatabase() {
		try {
			this.getDatabase().find(Flags.class).findRowCount();
		} catch (PersistenceException ex) {
			this.getLogger().log(Level.INFO, "Setting up new database...");
			this.installDDL();
			this.getLogger().log(Level.INFO, "Finished setting up database!");
		}
	}
}
