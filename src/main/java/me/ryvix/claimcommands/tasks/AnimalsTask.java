/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.tasks;

import static java.lang.System.currentTimeMillis;
import java.util.UUID;
import me.ryvix.claimcommands.ClaimCommands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Animals;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * MobsTask
 *
 * Checks claims for animals and removes them if necessary.
 */
public class AnimalsTask extends BukkitRunnable {

	private final ClaimCommands plugin;
	public static long millis = 0L;

	// Constructor
	public AnimalsTask(ClaimCommands plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {

		// once a set amount of time goes by save our spot for next time
		millis = currentTimeMillis();

		// check each world
		for (World world : plugin.getCcWorlds()) {

			// check each LivingEntity in the world
			for (LivingEntity entity : world.getLivingEntities()) {

				// check if entity is a animals first
				if (!(entity instanceof Animals)) {
					continue;
				}

				// don't remove this mob if it's already set to be removed
				if (plugin.getEntityUUIDs().contains(entity.getUniqueId())) {
					continue;
				}

				// get entity location
				Location location = entity.getLocation();

				// make sure chunk is loaded at that location
				if (!location.getChunk().isLoaded()) {
					continue;
				}
				
				me.ryanhamshire.GriefPrevention.Claim gpclaim = plugin.getCcClaim().getGPClaim(location);

				// check if location is in a claim
				if (plugin.getCcClaim().check(entity.getLocation())) {
					Long claimid = gpclaim.getID();
					
					// TODO: keep track of location, claimid, animals flag
					// check location, then claimid, then animals flag

					// check if claim has animals flag
					if (!plugin.getCcFlags().getAnimals(claimid)) {

						// store entity id so we dont try removing it twice
						UUID id = entity.getUniqueId();
						plugin.addEntityUUID(id);

						// remove entity
						entity.remove();

						// store entity id so we dont try removing it twice
						plugin.removeEntityUUID(id);
					}
				}

				// Keep going?
				long testMilli = currentTimeMillis();
				if(testMilli - millis > 100) {
					break;
				}
			}
		}
	}
}
