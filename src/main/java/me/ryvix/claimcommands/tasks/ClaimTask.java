/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.tasks;

import static java.lang.System.currentTimeMillis;
import java.util.Map.Entry;
import java.util.UUID;
import me.ryvix.claimcommands.functions.CheckPlayer;
import me.ryvix.claimcommands.ClaimCommands;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * ClaimTasks
 * 
 * Checks claim access for all players that have moved since last check.
 *
 * Will teleport players outside the lower corner who have no access to the claim they are in.
 */
public class ClaimTask extends BukkitRunnable {

	private final ClaimCommands plugin;
	private static long millis = 0L;
	//private static String last = "";

	// Constructor
	public ClaimTask(ClaimCommands plugin) {
		this.plugin = plugin;
	}

	/**
	 * @return the millis
	 */
	public static long getMillis() {
		return millis;
	}

	/**
	 * @param aMillis the millis to set
	 */
	public static void setMillis(long aMillis) {
		millis = aMillis;
	}

	@Override
	public void run() {

		// once a set amount of time goes by save our spot for next time
		setMillis(currentTimeMillis());

		// Loop through players that have moved
		for (Entry<UUID, CheckPlayer> checkPlayer : plugin.getCheckPlayers().entrySet()) {

			// Player name from MovedPlayer object
			UUID playerUUID = checkPlayer.getKey();

			// Don't check the player we checked last
			//if(last.equals(playerName)) {
			//	continue;
			//}

			// Player object from server
			final Player player = plugin.getServer().getPlayer(playerUUID);

			// If there is no player anymore go to next moved player
			if (player == null) {
				continue;
			}

			// Check player object
			CheckPlayer checkPlayerObj = checkPlayer.getValue();

			// Last location of the player
			Location lLocation = checkPlayerObj.getLocation();

			// Current location of the player
			Location cLocation = player.getLocation();

			// If the aren't in the same spot
			if (lLocation.getBlockX() != cLocation.getBlockX() || lLocation.getBlockY() != cLocation.getBlockY() || lLocation.getBlockZ() != cLocation.getBlockZ()) {

				// send entry/exit flags
				Boolean sendCEntry = false;
				Boolean sendLExit = false;

				// last and current claim checks
				boolean lClaim = plugin.getCcClaim().check(lLocation);
				boolean cClaim = plugin.getCcClaim().check(cLocation);

				// Is their current location inside a claim?
				if (cClaim) {

					// Check if player cannot enter the claim (is private or they are on deny list)
					// Let admins in claims
					boolean canEnter = plugin.getCcClaim().canEnter(player.getUniqueId(), cLocation);
					if (!canEnter && !player.hasPermission("claimcommands.admin")) {

						// Eject player from the claim (outside lower corner)
						//GriefPrevention gp = new GriefPrevention();
						//gp.ejectPlayer(player);
						plugin.getCcGP().ejectPlayer(player);

						// Tell them they have no access
						player.sendMessage(ChatColor.RED + "You aren't allowed there!");

						// No further checks are needed so return
						// do not remove player from movedPlayers HashMap because they were moved again
						return;

					} else {
						// They can enter the claim
						// Was their last location inside a different claim?
						long lClaimId = plugin.getCcClaim().getGPClaimId(lLocation);
						long cClaimId = plugin.getCcClaim().getGPClaimId(cLocation);

						// Are they still in a claim?
						if (lClaim && cClaim) {

							// Are they not in the same claim?
							if (lClaimId != cClaimId) {
								sendLExit = true;
								sendCEntry = true;
							}

						} else {
							// Are they not in the same claim?
							if (lClaimId != cClaimId) {
								sendCEntry = true;
							}
						}
					}

				} else {
					// They are not in a claim
					// Check if they were in a claim

					// Is their last location in a claim?
					if (lClaim && plugin.getCcClaim().canExit(player, lLocation)) {

						// They came out of a claim so send exit message
						sendLExit = true;
					}
				}

				// Exit message of last claim
				if (sendLExit) {
					long lClaimId = plugin.getCcClaim().getGPClaimId(lLocation);
					String exitMsg = plugin.getCcFlags().getExitMsg(lClaimId);
					if (!exitMsg.isEmpty()) {
						player.sendMessage(ChatColor.translateAlternateColorCodes("&".charAt(0), exitMsg));
					}
				}

				// Entry message of current claim
				if (sendCEntry) {
					long cClaimId = plugin.getCcClaim().getGPClaimId(cLocation);
					String entryMsg = plugin.getCcFlags().getEntryMsg(cClaimId);
					if (!entryMsg.isEmpty()) {
						player.sendMessage(ChatColor.translateAlternateColorCodes("&".charAt(0), entryMsg));
					}
				}
			}

			// Remove player from movedPlayers HashMap
			plugin.removeCheckPlayer(playerUUID);

			// Keep going?
			long testMilli = currentTimeMillis();
			if(testMilli - getMillis() > 100) {
				//last = playerName;
				break;
			}
		}
	}
}
