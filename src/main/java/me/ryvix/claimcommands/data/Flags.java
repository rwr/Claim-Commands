/**
 * Claim Commands - Provides more control over Grief Prevention claims.
 * Copyright (C) 2013-2016 Ryan Rhode - rrhode@gmail.com
 *
 * The MIT License (MIT) - See LICENSE.txt
 *
 */
package me.ryvix.claimcommands.data;

import com.avaje.ebean.validation.Length;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name="flags")
public class Flags {
    @Id
    private int id;
	
    private int claimid;

    @Length(max=20)
    private String flag;

    @Length(max=100)
    private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setClaimid(int claimid) {
        this.claimid = claimid;
    }

    public int getClaimid() {
        return claimid;
    }
	
	public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }
	
	public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}